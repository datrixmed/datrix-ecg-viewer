
#define MyManufacturer   "Datrix"
#define MyDevFolder  	 "../out"

#define MyAppName		"DatrixECGViewer"
#define MyAppURL		"http://www.datrixmed.com"
#define MyAppExeName	"DatrixECGViewer"
#define MyVersion		"5d949d"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppID={{dabdab5e-8d1c-4dab-ae4c-3dab75dabfe9}}
AppName={#MyAppName}
AppVerName={#MyAppName}
AppPublisher={#MyManufacturer}
AppPublisherURL={#MyAppURL}
AppVersion={#MyVersion}
DefaultDirName={commonpf}\{#MyManufacturer} ${#MyAppExeName}
DefaultGroupName={#MyAppName}
UninstallDisplayIcon={app}\{#MyAppExeName}.exe
Compression=lzma/Ultra
SolidCompression=yes
OutputBaseFilename=Setup-{#MyAppExeName}-{#MyVersion}
OutputDir=.
VersionInfoDescription={#MyAppName}(Qt 5.13.0)
AppCopyright=(C) Copyright 2021, {#MyManufacturer}


[Dirs]
Name: "{app}";

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}.exe"; WorkingDir: "{app}"
Name: "{commonstartup}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}.exe"
Name: "{group}\Uninstall"; Filename: "{uninstallexe}"

[Files]
Source: "{#MyDevFolder}/{#MyAppExeName}.exe"; Excludes: ".svn,media"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; Qt platforms
Source: "{#MyDevFolder}/platforms/qwindows.dll"; DestDir: "{app}/platforms";


[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, "&", "&&")}}"; Flags: nowait postinstall skipifsilent


